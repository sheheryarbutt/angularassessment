(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<!-- <div style=\"\" id=\"banner\"> -->\r\n  <h1 style=\"\r\n  text-align:center; \r\n  vertical-align:middle;\r\n  line-height: 70px;\r\n  font-family: 'starwars';\r\n  margin:0;\r\n  height:80px;\r\n    color:yellow;\r\n    background-color: black;\r\n    /* background-image: url('../assets/navbg.jpg');\r\n    background-size:cover; */\r\n \r\n   \">\r\n    {{title}}\r\n  </h1>\r\n  \r\n  <div style='text-align:center; '> \r\n  \r\n  <!-- <select (change)=\"updateAppRender()\" [(ngModel)]='selectedType'> -->\r\n    Type:<select [(ngModel)]=\"selectedType\" >\r\n      <option *ngFor=\"let type of selectionType\" [value]=\"type\">{{type}}</option>\r\n      <!-- <option>TEST</option> -->\r\n    </select>\r\n\r\n    Id:<select [(ngModel)]=\"typeNumber\">\r\n        <option *ngFor=\"let number of [1,2,3,4,5,6]\" [value]=\"number\">{{number}}</option>\r\n        <!-- <option>TEST</option> -->\r\n      </select>\r\n      \r\n      <button [disabled]='selectedType==undefined || typeNumber==undefined' (click)=\"onSubmitRequest()\" >SEARCH</button>\r\n      <button (click)=\"showHistory()\" >HISTORY</button>\r\n      <h3 *ngIf=\"showComponenets.error\">Error Retrieving Data</h3>\r\n      <div *ngIf='showComponenets.history'>\r\n        <h1>HISTORY</h1>\r\n        <app-history-render \r\n        *ngFor=\"let history of searchHistory\" \r\n        [history]='history'\r\n        (fillFormFields)=fillFormFieldstHandler($event)\r\n        ></app-history-render>\r\n      </div>\r\n      <app-people-render *ngIf=\"showComponenets.people\" [people]='selectedTypeData' [film]='film' ></app-people-render>\r\n      <app-planet-render *ngIf=\"showComponenets.planet\" [planet]='selectedTypeData' [film]='film'></app-planet-render>\r\n      <app-specie-render *ngIf=\"showComponenets.specie\" [specie]='selectedTypeData' [film]='film'></app-specie-render>\r\n      <app-starship-render *ngIf=\"showComponenets.starship\" [starship]='selectedTypeData' [film]='film'></app-starship-render>\r\n      <app-vehicle-render *ngIf=\"showComponenets.vehicle\" [vehicle]='selectedTypeData' [film]='film'></app-vehicle-render>\r\n    </div>\r\n<!-- </div> -->\r\n\r\n\r\n\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/history-render/history-render.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/history-render/history-render.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"width: 300px; border: 5px solid; display:inline-block; margin:10px; padding:10px\">\n<h3 > \n    <h4>Type: {{history.type}}</h4> <h4>Id: {{history.id}}</h4>  <h4>Date: {{history.time | date: 'medium'}}</h4>  \n    <button (click)='emitfillFormFields(history)' style=\"background-color:grey; color:white; padding: 8px 20px; text-align: center; display:inline-block;font-size:13px; \"> Search Again</button>\n</h3>\n\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/people-render/people-render.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/people-render/people-render.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n\n    <div style=\"width:400px; border: 5px solid; padding: 25px; margin: 20px; position:absolute; left:35%\">\n    <h3>Name: {{people.name}} </h3>\n    <h3>Gender: {{people.gender}}</h3>\n    <h3>birth year: {{people.birth_year}} </h3>\n    <h3>Film title: {{film.title}}</h3>\n    </div>\n\n</aside>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/planet-render/planet-render.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/planet-render/planet-render.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n\n        <div style=\"width:400px; border: 5px solid; padding: 25px; margin: 20px; position:absolute; left:35%\">\n        <h3>Name:{{planet.name}} </h3>\n        <h3>rotation_period: {{planet.rotation_period}}</h3>\n        <h3>diameter: {{planet.diameters}}</h3>\n        <h3>gravity: {{planet.gravity}} </h3>\n        <h3>Film title: {{film.title}}</h3>\n        </div>\n    \n\n    </aside>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/specie-render/specie-render.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/specie-render/specie-render.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n\n        <div style=\"width:400px; border: 5px solid; padding: 25px; margin: 20px; position:absolute; left:35%\">\n        <h3> Name:{{specie.name}} </h3>\n        <h3>classification: {{specie.classification}} </h3>\n        <h3>designation: {{specie.designation}} </h3>\n        <h3>average lifespan: {{specie.average_lifespan}}</h3>\n        <h3>Film title: {{film.title}}</h3>\n        </div>\n    \n\n    </aside>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/starship-render/starship-render.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/starship-render/starship-render.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n\n        <div style=\"width:400px; border: 5px solid; padding: 25px; margin: 20px; position:absolute; left:35%\">\n     \n            <h3>Name:{{starship.name}} </h3>\n        <h3>model: {{starship.model}}</h3>\n        <h3>manufacturer: {{starship.manufacturer}} </h3>\n        <h3> cargo capacity: {{starship.cargo_capacity}}</h3>\n        <h3>Film title: {{film.title}}</h3>\n        </div>\n    \n\n    </aside>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/renderers/vehicle-render/vehicle-render.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/renderers/vehicle-render/vehicle-render.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n\n        <div style=\"width:400px; border: 5px solid; padding: 25px; margin: 20px; position:absolute; left:35%\">\n        <h3>Name:{{vehicle.name}}</h3>\n        <h3>model: {{vehicle.model}}</h3>\n        <h3>manufacturer: {{vehicle.manufacturer}} </h3>\n        <h3>vehicle_class: {{vehicle.vehicle_class}}</h3>\n        <h3>Film title: {{film.title}}</h3>\n        </div>\n    \n\n    </aside>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\r\n    margin: 10px\r\n}\r\n\r\n@font-face{\r\n    font-family: 'starwars';\r\n    src: url('Starjhol.ttf')\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtBQUNKOztBQUVBO0lBQ0ksdUJBQXVCO0lBQ3ZCO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XHJcbiAgICBtYXJnaW46IDEwcHhcclxufVxyXG5cclxuQGZvbnQtZmFjZXtcclxuICAgIGZvbnQtZmFtaWx5OiAnc3RhcndhcnMnO1xyXG4gICAgc3JjOiB1cmwoJy4uL2Fzc2V0cy9TdGFyamhvbC50dGYnKVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _retriever_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./retriever.service */ "./src/app/retriever.service.ts");



let AppComponent = class AppComponent {
    constructor(retieverService) {
        this.retieverService = retieverService;
        this.title = "STAR WARS DATABASE";
        this.selectionType = [
            "people",
            "planets",
            "vehicles",
            "species",
            "starships"
        ];
        this.showComponenets = { people: false, planet: false, vehicle: false,
            specie: false, starship: false, error: false, history: false };
        this.searchHistory = [];
    }
    ngOnInit() {
    }
    //function when form gets submitted (we want to retreieve the data)
    onSubmitRequest() {
        console.log("selected type: " + this.selectedType + " type number: " + this.typeNumber);
        this.retieverService.getData(this.selectedType, this.typeNumber).subscribe((data) => {
            //set the data and push information into history
            this.selectedTypeData = data;
            this.searchHistory.push({ time: new Date(), type: this.selectedType, id: this.typeNumber });
            //make a second call for getting film(s)-- can easily be extended to get more than one film
            this.retieverService.getFilmData(data.films[0]).subscribe((data) => {
                this.film = data;
                //set all componenets to false (so nothing is displayed)
                this.setAllComponenetsFalse();
                //then display the correct component
                this.displayCorrectComponent();
            });
        }, (error) => {
            //In case of errors thrown by the api call, the code comes here and launches a message.
            // The backup function goes and retrieves all the data for example for vehicles and then extracts the list of all vehicles and uses
            //the number to server as an index and retrieves the particular entry
            this.setAllComponenetsFalse();
            this.showComponenets.error = true;
            //alert("Data Extraction Error. Please try again later.")
            this.backup(this.selectedType, this.typeNumber);
        });
    }
    backup(category, id) {
        //The logic is there to extract this information and it is printed to console, however we made a design decision to simply
        //alert the user that the API call is not working.
        this.retieverService.getBackupData(category).subscribe((data) => {
            let var1 = JSON.parse(JSON.stringify(data));
            console.log(var1.results[Number(id)]);
        });
    }
    // displays the history screen
    showHistory() {
        this.setAllComponenetsFalse();
        this.showComponenets.history = true;
    }
    // get data from child component history and fill in fields for parent (app.component)
    fillFormFieldstHandler(data) {
        //change data and invoke a new search from one of the items of the history
        this.selectedType = data.type;
        this.typeNumber = data.id;
        this.onSubmitRequest();
    }
    //helper function to remove all components before showing a specific one
    setAllComponenetsFalse() {
        this.showComponenets.people = false;
        this.showComponenets.planet = false;
        this.showComponenets.vehicle = false;
        this.showComponenets.specie = false;
        this.showComponenets.starship = false;
        this.showComponenets.error = false;
        this.showComponenets.history = false;
    }
    // only display the component which is active rn
    displayCorrectComponent() {
        switch (this.selectedType) {
            case "people":
                this.showComponenets.people = true;
                break;
            case "planets":
                this.showComponenets.planet = true;
                break;
            case "vehicles":
                this.showComponenets.vehicle = true;
                break;
            case "species":
                this.showComponenets.specie = true;
                break;
            case "starships":
                this.showComponenets.starship = true;
                break;
            default:
                break;
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _retriever_service__WEBPACK_IMPORTED_MODULE_2__["RetrieverService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _renderers_people_render_people_render_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./renderers/people-render/people-render.component */ "./src/app/renderers/people-render/people-render.component.ts");
/* harmony import */ var _renderers_planet_render_planet_render_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./renderers/planet-render/planet-render.component */ "./src/app/renderers/planet-render/planet-render.component.ts");
/* harmony import */ var _renderers_specie_render_specie_render_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./renderers/specie-render/specie-render.component */ "./src/app/renderers/specie-render/specie-render.component.ts");
/* harmony import */ var _renderers_starship_render_starship_render_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./renderers/starship-render/starship-render.component */ "./src/app/renderers/starship-render/starship-render.component.ts");
/* harmony import */ var _renderers_vehicle_render_vehicle_render_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./renderers/vehicle-render/vehicle-render.component */ "./src/app/renderers/vehicle-render/vehicle-render.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _renderers_history_render_history_render_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./renderers/history-render/history-render.component */ "./src/app/renderers/history-render/history-render.component.ts");














let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _renderers_people_render_people_render_component__WEBPACK_IMPORTED_MODULE_7__["PeopleRenderComponent"],
            _renderers_planet_render_planet_render_component__WEBPACK_IMPORTED_MODULE_8__["PlanetRenderComponent"],
            _renderers_specie_render_specie_render_component__WEBPACK_IMPORTED_MODULE_9__["SpecieRenderComponent"],
            _renderers_starship_render_starship_render_component__WEBPACK_IMPORTED_MODULE_10__["StarshipRenderComponent"],
            _renderers_vehicle_render_vehicle_render_component__WEBPACK_IMPORTED_MODULE_11__["VehicleRenderComponent"],
            _renderers_history_render_history_render_component__WEBPACK_IMPORTED_MODULE_13__["HistoryRenderComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/renderers/history-render/history-render.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/renderers/history-render/history-render.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy9oaXN0b3J5LXJlbmRlci9oaXN0b3J5LXJlbmRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/renderers/history-render/history-render.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/renderers/history-render/history-render.component.ts ***!
  \**********************************************************************/
/*! exports provided: HistoryRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryRenderComponent", function() { return HistoryRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HistoryRenderComponent = class HistoryRenderComponent {
    constructor() {
        this.fillFormFields = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); //creating your own event to communicate with the parent
    }
    ngOnInit() {
    }
    emitfillFormFields(history) {
        this.fillFormFields.emit(history);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HistoryRenderComponent.prototype, "history", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], HistoryRenderComponent.prototype, "fillFormFields", void 0);
HistoryRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history-render',
        template: __webpack_require__(/*! raw-loader!./history-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/history-render/history-render.component.html"),
        styles: [__webpack_require__(/*! ./history-render.component.css */ "./src/app/renderers/history-render/history-render.component.css")]
    })
], HistoryRenderComponent);



/***/ }),

/***/ "./src/app/renderers/people-render/people-render.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/renderers/people-render/people-render.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy9wZW9wbGUtcmVuZGVyL3Blb3BsZS1yZW5kZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/renderers/people-render/people-render.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/renderers/people-render/people-render.component.ts ***!
  \********************************************************************/
/*! exports provided: PeopleRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeopleRenderComponent", function() { return PeopleRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PeopleRenderComponent = class PeopleRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], PeopleRenderComponent.prototype, "people", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], PeopleRenderComponent.prototype, "film", void 0);
PeopleRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-people-render',
        template: __webpack_require__(/*! raw-loader!./people-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/people-render/people-render.component.html"),
        styles: [__webpack_require__(/*! ./people-render.component.css */ "./src/app/renderers/people-render/people-render.component.css")]
    })
], PeopleRenderComponent);



/***/ }),

/***/ "./src/app/renderers/planet-render/planet-render.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/renderers/planet-render/planet-render.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy9wbGFuZXQtcmVuZGVyL3BsYW5ldC1yZW5kZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/renderers/planet-render/planet-render.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/renderers/planet-render/planet-render.component.ts ***!
  \********************************************************************/
/*! exports provided: PlanetRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanetRenderComponent", function() { return PlanetRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PlanetRenderComponent = class PlanetRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], PlanetRenderComponent.prototype, "planet", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], PlanetRenderComponent.prototype, "film", void 0);
PlanetRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-planet-render',
        template: __webpack_require__(/*! raw-loader!./planet-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/planet-render/planet-render.component.html"),
        styles: [__webpack_require__(/*! ./planet-render.component.css */ "./src/app/renderers/planet-render/planet-render.component.css")]
    })
], PlanetRenderComponent);



/***/ }),

/***/ "./src/app/renderers/specie-render/specie-render.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/renderers/specie-render/specie-render.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy9zcGVjaWUtcmVuZGVyL3NwZWNpZS1yZW5kZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/renderers/specie-render/specie-render.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/renderers/specie-render/specie-render.component.ts ***!
  \********************************************************************/
/*! exports provided: SpecieRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecieRenderComponent", function() { return SpecieRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SpecieRenderComponent = class SpecieRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], SpecieRenderComponent.prototype, "specie", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], SpecieRenderComponent.prototype, "film", void 0);
SpecieRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-specie-render',
        template: __webpack_require__(/*! raw-loader!./specie-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/specie-render/specie-render.component.html"),
        styles: [__webpack_require__(/*! ./specie-render.component.css */ "./src/app/renderers/specie-render/specie-render.component.css")]
    })
], SpecieRenderComponent);



/***/ }),

/***/ "./src/app/renderers/starship-render/starship-render.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/renderers/starship-render/starship-render.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy9zdGFyc2hpcC1yZW5kZXIvc3RhcnNoaXAtcmVuZGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/renderers/starship-render/starship-render.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/renderers/starship-render/starship-render.component.ts ***!
  \************************************************************************/
/*! exports provided: StarshipRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarshipRenderComponent", function() { return StarshipRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let StarshipRenderComponent = class StarshipRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], StarshipRenderComponent.prototype, "starship", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], StarshipRenderComponent.prototype, "film", void 0);
StarshipRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-starship-render',
        template: __webpack_require__(/*! raw-loader!./starship-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/starship-render/starship-render.component.html"),
        styles: [__webpack_require__(/*! ./starship-render.component.css */ "./src/app/renderers/starship-render/starship-render.component.css")]
    })
], StarshipRenderComponent);



/***/ }),

/***/ "./src/app/renderers/vehicle-render/vehicle-render.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/renderers/vehicle-render/vehicle-render.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlbmRlcmVycy92ZWhpY2xlLXJlbmRlci92ZWhpY2xlLXJlbmRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/renderers/vehicle-render/vehicle-render.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/renderers/vehicle-render/vehicle-render.component.ts ***!
  \**********************************************************************/
/*! exports provided: VehicleRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleRenderComponent", function() { return VehicleRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let VehicleRenderComponent = class VehicleRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], VehicleRenderComponent.prototype, "vehicle", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], VehicleRenderComponent.prototype, "film", void 0);
VehicleRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-vehicle-render',
        template: __webpack_require__(/*! raw-loader!./vehicle-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/renderers/vehicle-render/vehicle-render.component.html"),
        styles: [__webpack_require__(/*! ./vehicle-render.component.css */ "./src/app/renderers/vehicle-render/vehicle-render.component.css")]
    })
], VehicleRenderComponent);



/***/ }),

/***/ "./src/app/retriever.service.ts":
/*!**************************************!*\
  !*** ./src/app/retriever.service.ts ***!
  \**************************************/
/*! exports provided: RetrieverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RetrieverService", function() { return RetrieverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");





let RetrieverService = class RetrieverService {
    constructor(http) {
        this.http = http;
        this.baseUrl = "https://swapi.co/api/";
    }
    handleError(error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('Something bad happened; please try again later.');
    }
    getData(category, id) {
        return this.http.get(this.baseUrl + category + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getFilmData(filmUrl) {
        return this.http.get(filmUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getBackupData(category) {
        return this.http.get(this.baseUrl + category)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
};
RetrieverService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
RetrieverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], RetrieverService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Administrator\Desktop\FrontEndWork\angularTraining\AngularAssessment\angAssessment\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map