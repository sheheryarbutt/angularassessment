// import { Portfolio } from './portfolio';

//declare a class; 'export' tells javascript we can export this class to others xD
export class Specie {

    //class properties
    name:string;
    classification: string       
    designation:string        
    average_height:string       
    skin_colors:string         
    hair_colors:string        
    eye_colors:string       
    average_lifespan:string        
    homeworld:string;
    language:string;
    people:string[];
    films:string[];
    created:string;
    edited:string;
    url:string;


    constructor() {
        
    }
    
}