// import { Portfolio } from './portfolio';

//declare a class; 'export' tells javascript we can export this class to others xD
export class People {

    //class properties
    name:string;
    height:number;
    mass:number;
    hair_color:string;
    skin_color:string;
    eye_color:string;
    birth_year:string;
    gender:string;
    homeworld:string;
    films:string[];
    species:string[];
    vehicles:string[]
    starships:string[]
    created:string;
    edited:string;
    url:string;
    isInstance="people";


    constructor() {
        
    }
    
}