// import { Portfolio } from './portfolio';

//declare a class; 'export' tells javascript we can export this class to others xD
export class Film {

    //class properties
    title:string;
    episode_id:number;
    opening_crawl:number;
    director:string;
    producer:string;
    release_date:string;
    characters:string[];
    planets:string[];
    starships:string[]
    vehicles:string[]
    species:string[]
    created:string;
    edited:string;
    url:string;
  


    constructor() {
        
    }
    
}