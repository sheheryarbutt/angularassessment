// import { Portfolio } from './portfolio';

//declare a class; 'export' tells javascript we can export this class to others xD
export class Planet {

    //class properties
    name:string;
    rotation_period: string;
    orbital_period: string       
    diameter:string        
    climate:string       
    gravity:string         
    terrain:string        
    surface_water:string       
    population:string        
    residents:string;
    films:string[];
    created:string;
    edited:string;
    url:string;


    constructor() {
        
    }
    
}