import { Component, OnInit, Input } from '@angular/core';
import { People } from 'src/app/models/people';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-people-render',
  templateUrl: './people-render.component.html',
  styleUrls: ['./people-render.component.css']
})
export class PeopleRenderComponent implements OnInit {
  @Input() people: People
  @Input() film: Film
  constructor() { }

  ngOnInit() {
  }

}
