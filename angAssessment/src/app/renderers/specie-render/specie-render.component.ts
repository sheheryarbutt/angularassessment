import { Component, OnInit, Input } from '@angular/core';
import { Specie } from 'src/app/models/specie';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-specie-render',
  templateUrl: './specie-render.component.html',
  styleUrls: ['./specie-render.component.css']
})
export class SpecieRenderComponent implements OnInit {
  @Input() specie:Specie
  @Input() film:Film
  constructor() { }

  ngOnInit() {
  }

}
