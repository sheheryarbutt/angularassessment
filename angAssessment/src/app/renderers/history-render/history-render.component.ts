import { Component, OnInit, Output, Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-history-render',
  templateUrl: './history-render.component.html',
  styleUrls: ['./history-render.component.css']
})
export class HistoryRenderComponent implements OnInit {
  @Input() history

  @Output() fillFormFields = new EventEmitter() //creating your own event to communicate with the parent

  constructor() { }

  ngOnInit() {
  }

  emitfillFormFields(history){
    this.fillFormFields.emit(history);
  }

}
