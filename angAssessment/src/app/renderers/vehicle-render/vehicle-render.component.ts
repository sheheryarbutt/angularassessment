import { Component, OnInit, Input } from '@angular/core';
import { Vehicle } from 'src/app/models/vehicle';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-vehicle-render',
  templateUrl: './vehicle-render.component.html',
  styleUrls: ['./vehicle-render.component.css']
})
export class VehicleRenderComponent implements OnInit {
  @Input() vehicle:Vehicle
  @Input() film:Film
  constructor() { }

  ngOnInit() {
  }

}
