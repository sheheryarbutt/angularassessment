import { Component, OnInit, Input } from '@angular/core';
import { Planet } from 'src/app/models/planet';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-planet-render',
  templateUrl: './planet-render.component.html',
  styleUrls: ['./planet-render.component.css']
})
export class PlanetRenderComponent implements OnInit {
  @Input() planet: Planet
  @Input() film: Film
  constructor() { }

  ngOnInit() {
  }

}



