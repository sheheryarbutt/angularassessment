import { Component, OnInit, Input } from '@angular/core';
import { Starship } from 'src/app/models/starship';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-starship-render',
  templateUrl: './starship-render.component.html',
  styleUrls: ['./starship-render.component.css']
})
export class StarshipRenderComponent implements OnInit {
  @Input() starship:Starship
  @Input() film:Film
  constructor() { }

  ngOnInit() {
  }

}
