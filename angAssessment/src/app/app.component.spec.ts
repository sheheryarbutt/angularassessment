import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http';
import { PeopleRenderComponent } from './renderers/people-render/people-render.component';
import { PlanetRenderComponent } from './renderers/planet-render/planet-render.component';
import { SpecieRenderComponent } from './renderers/specie-render/specie-render.component';
import { StarshipRenderComponent } from './renderers/starship-render/starship-render.component';
import { VehicleRenderComponent } from './renderers/vehicle-render/vehicle-render.component';
import { HistoryRenderComponent } from './renderers/history-render/history-render.component';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent,
        PeopleRenderComponent,
        PlanetRenderComponent,
        SpecieRenderComponent,
        StarshipRenderComponent,
        VehicleRenderComponent,
        HistoryRenderComponent
      ],

    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angAssessment'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('STAR WARS DATABASE');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('STAR WARS DATABASE');
  });
});
