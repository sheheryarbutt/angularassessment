import { Component, OnInit } from '@angular/core';
import { RetrieverService } from './retriever.service';
import { Observable } from 'rxjs';
import { People } from './models/people';
import { Film } from './models/film';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = "STAR WARS DATABASE"
  selectionType=[
     "people", 
    "planets",
   "vehicles",
    "species", 
    "starships"
  ]
  selectedType:string
  typeNumber:string
  film:Film
  selectedTypeData
  showComponenets={people:false, planet:false, vehicle:false,
  specie:false, starship:false, error:false, history:false}
  searchHistory = []



  
  constructor(private retieverService: RetrieverService){  

  }
  ngOnInit(){

  }

  
  //function when form gets submitted (we want to retreieve the data)
  onSubmitRequest(){
    console.log("selected type: " + this.selectedType + " type number: " + this.typeNumber)
    this.retieverService.getData(this.selectedType, this.typeNumber).subscribe((data)=>{
      //set the data and push information into history
      this.selectedTypeData = data
      this.searchHistory.push({time:new Date(), type: this.selectedType, id:this.typeNumber})
      //make a second call for getting film(s)-- can easily be extended to get more than one film
      this.retieverService.getFilmData(data.films[0]).subscribe((data:Film)=>{
        this.film = data
          //set all componenets to false (so nothing is displayed)
        this.setAllComponenetsFalse();
        //then display the correct component
        this.displayCorrectComponent()
      })
    }, (error) => {
      //In case of errors thrown by the api call, the code comes here and launches a message.

      // The backup function goes and retrieves all the data for example for vehicles and then extracts the list of all vehicles and uses
      //the number to server as an index and retrieves the particular entry
      this.setAllComponenetsFalse()
      this.showComponenets.error = true;
      //alert("Data Extraction Error. Please try again later.")
      this.backup(this.selectedType, this.typeNumber)
      
    })
    
  }

  backup(category:string, id: string) {
    //The logic is there to extract this information and it is printed to console, however we made a design decision to simply
    //alert the user that the API call is not working.
    this.retieverService.getBackupData(category).subscribe((data)=>{
      let var1 =JSON.parse(JSON.stringify(data))
      console.log(var1.results[Number(id)])
    })
  }

// displays the history screen
  showHistory(){
    this.setAllComponenetsFalse();
    this.showComponenets.history = true;
  }
// get data from child component history and fill in fields for parent (app.component)
  fillFormFieldstHandler(data){
    //change data and invoke a new search from one of the items of the history
    this.selectedType = data.type;
    this.typeNumber = data.id;
    this.onSubmitRequest();
  }

  //helper function to remove all components before showing a specific one
  setAllComponenetsFalse(){
    this.showComponenets.people=false;
    this.showComponenets.planet=false;
    this.showComponenets.vehicle=false;
    this.showComponenets.specie=false;
    this.showComponenets.starship=false;
    this.showComponenets.error=false;
    this.showComponenets.history=false;
  }
  // only display the component which is active rn
  displayCorrectComponent(){
    switch(this.selectedType) {
      case "people":
        this.showComponenets.people = true;
        break;
      case "planets":
          this.showComponenets.planet = true;
          break;
      case "vehicles":
          this.showComponenets.vehicle = true;
          break;
      case "species":
          this.showComponenets.specie = true;
          break;
      case "starships":
        this.showComponenets.starship = true;
        break;
      default:
        break;
      } 
  }


}
