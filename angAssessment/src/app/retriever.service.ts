import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { People } from './models/people'
import { Planet } from './models/planet'
import { Vehicle } from './models/vehicle';
import { Starship } from './models/starship';
import { Specie } from './models/specie';
import { Film } from './models/film';

@Injectable({
  providedIn: 'root'
})
export class RetrieverService {
  baseUrl:string = "https://swapi.co/api/"

  constructor(private http: HttpClient) { 

  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }


getData(category:string, id:string):Observable<People | Planet | Specie | Starship | Vehicle> {
    
       
    return this.http.get<People | Planet | Specie | Starship | Vehicle>(this.baseUrl+ category +"/"+id)
    .pipe(retry(3),
      catchError(this.handleError)
    );
}

getFilmData(filmUrl):Observable<Film>{
  return this.http.get<Film>(filmUrl)
  .pipe(retry(3), catchError(this.handleError))
}

getBackupData(category:string):Observable<People | Planet | Specie | Starship | Vehicle> {
    
       
  return this.http.get<People | Planet | Specie | Starship | Vehicle>(this.baseUrl+ category)
  .pipe(retry(3),
    catchError(this.handleError)
  );
}

}