import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http';
import { PeopleRenderComponent } from './renderers/people-render/people-render.component';
import { PlanetRenderComponent } from './renderers/planet-render/planet-render.component';
import { SpecieRenderComponent } from './renderers/specie-render/specie-render.component';
import { StarshipRenderComponent } from './renderers/starship-render/starship-render.component';
import { VehicleRenderComponent } from './renderers/vehicle-render/vehicle-render.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HistoryRenderComponent } from './renderers/history-render/history-render.component';


@NgModule({
  declarations: [
    AppComponent,
    PeopleRenderComponent,
    PlanetRenderComponent,
    SpecieRenderComponent,
    StarshipRenderComponent,
    VehicleRenderComponent,
    HistoryRenderComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

